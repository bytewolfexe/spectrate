{ pkgs, lib, rustPlatform }: rustPlatform.buildRustPackage {
	pname = "spectrate";

	version = "1.0.0";

	src = ./.;

	nativeBuildInputs = [ pkgs.cargo pkgs.rustc ];
	cargoSha256 = "sha256-MebP5FRYDFG7U1mX+akVLrNGwvYQGAXoAEVwHCLY7Ng=";

	buildPhase = ''
	cargo build --release
	'';

	installPhase = ''
	mkdir -p $out/bin
	cp target/release/spectrate $out/bin
	'';
}
