mod window;
mod renderer;
mod helper;

pub use helper::*;

use std::borrow::Cow;
use std::io::Write;
use std::path::PathBuf;

use arboard::{Clipboard, ImageData};
use crossterm::cursor::MoveTo;
use crossterm::event::PopKeyboardEnhancementFlags;
use crossterm::execute;
use crossterm::terminal::{disable_raw_mode, LeaveAlternateScreen};
use image::imageops::FilterType;
use image::{DynamicImage, EncodableLayout};
use owo_colors::OwoColorize;
use renderer::common::{Renderer, Rendering};

#[allow(dead_code)]
#[derive(Debug)]
pub struct State {
    offset_x: i16,
    offset_y: i16,
    zoom: f32,
    images: Vec<(String, DynamicImage)>,
    current_image: usize,
    working_dir: PathBuf,
}

impl State {
    pub fn load_dir(path: &PathBuf) -> Result<Self, String> {
        let current_dir = path;

        // Now to get the directory's content (and possibly, the searched file's name)
        let (name, dir) = if current_dir.is_file() {
            let file = match current_dir.file_name() {
                Some(f) => f,
                None => return Err(String::from("Unknown error ...")),
            };
            let parent = match current_dir.parent() {
                Some(p) => p,
                None => return Err(String::from("Unknown error ...")),
            };
            (Some(file), parent)
        } else {
            (None, current_dir.as_path())
        };

        let dir_content_iter = match std::fs::read_dir(dir) {
            Ok(iter) => iter,
            Err(_) => {
                return Err(format!(
                    "Could not read directory: {}",
                    dir.as_os_str().to_str().unwrap_or("???")
                ))
            }
        };

        // Collecting and initial filtering of directories.
        let mut dir_content_ordered = dir_content_iter
            .flatten()
            .map(|i| i.path())
            .collect::<Vec<_>>();
        // Alphabetically (?) sorting directories.
        dir_content_ordered.sort();

        // Getting first image's position
        let pos = match name {
            Some(n) => dir_content_ordered
                .iter()
                .position(|i| {
                    let this = i
                        .file_name()
                        .unwrap_or_default()
                        .to_str()
                        .unwrap_or_default();
                    let that = n;
                    this == that
                })
                .unwrap_or(0),
            None => 0,
        };

        // Loading images' data and storing it all into vector.
        let image_list = dir_content_ordered
            .into_iter()
            .map(|dir| {
                // List of files already filtered, so file_name() should not panic.
                let fname = String::from(dir.file_name().unwrap().to_str().unwrap());
                let reader = match image::io::Reader::open(dir) {
                    Ok(r) => match r.with_guessed_format() {
                        Ok(r) => Some(r),
                        Err(_) => None,
                    },
                    Err(_) => None,
                };

                match reader {
                    Some(r) => match r.decode() {
                        Ok(img_data) => Some((fname, img_data)),
                        Err(_) => None,
                    },
                    None => None,
                }
            })
            .flatten()
            .collect::<Vec<_>>();

        Ok(Self {
            offset_x: 0,
            offset_y: 0,
            zoom: 1.0,
            images: image_list,
            current_image: pos,
            working_dir: dir.to_path_buf(),
        })
    }

    pub fn next_image(&mut self) {
        // Image loopback - if end of list is reached, loop to beginning.
        if self.current_image == self.images.len() - 1 {
            self.current_image = 0;
        } else {
            self.current_image += 1;
        }
    }

    pub fn previous_image(&mut self) {
        // Image loopback - if beginning of list is reached, loop to end.
        if self.current_image == 0 {
            self.current_image = self.images.len() - 1;
        } else {
            self.current_image -= 1;
        }
    }

    pub fn zoom_in(&mut self) {
        self.zoom += 0.25;
        if self.zoom >= 10.0 {
            self.zoom = 10.0;
        }
    }

    pub fn zoom_out(&mut self) {
        self.zoom -= 0.25;
        if self.zoom <= 0.15 {
            self.zoom = 0.15;
        }
    }

    pub fn move_image(&mut self, x: i16, y: i16) {
        self.offset_x += x;
        self.offset_y += y;
    }

    pub fn reset(&mut self) {
        self.offset_x = 0;
        self.offset_y = 0;
        self.zoom = 1.0;
    }

    pub fn len(&self) -> usize {
        self.images.len()
    }

    pub fn get_current_image_data(&self) -> &image::DynamicImage {
        &self.images.get(self.current_image).unwrap().1
    }

    pub fn get_current_image_name(&self) -> String {
        match &self.images.get(self.current_image) {
            Some(img) => img.0.clone(),
            None => String::from(" ")
        }
    }

    pub fn draw_current_image_beta(&self) -> Result<(), String> {
        let img = match self.images.get(self.current_image) {
            Some(i) => &i.1,
            None => return Err(format!("Failed to open image! Reason : {}", self.get_current_image_name())),
        };
        let terminal_size = crossterm::terminal::size().unwrap_or_default();

        // Just simple float convertions.
        let img_width_f = img.width() as f32;
        let img_height_f = img.height() as f32;

        // Ratio is used for calculating one dimension from another.
        let res_ratio = img_width_f as f32 / img_height_f as f32;

        let width =
            ((terminal_size.0 / 4) as f32 * self.zoom) as u32;
        let height = (width as f32 / res_ratio) as u32;

        let scaled = img
            .resize_exact(width, height, FilterType::Nearest)
            .to_rgba8();
        // New scaled-down image's resolution.
        let size_w = scaled.width();
        let size_h = scaled.height();


        let mut origin = (
            (terminal_size.0 / 2) as i16 - (size_w / 2) as i16 + self.offset_x,
            (terminal_size.1 / 2) as i16 - (size_h / 2) as i16 + self.offset_y,
        );
        origin.1 = -origin.1;

        Renderer::draw_image(origin.0, origin.1, size_w, size_h, img);

        Ok(())
    }

    pub fn draw_current_image(&self) -> Result<(), String> {
        let img = match self.images.get(self.current_image) {
            Some(i) => &i.1,
            None => return Err(format!("Failed to open image! Reason : {}", self.get_current_image_name())),
        };

        // Just simple float convertions.
        let img_width_f = img.width() as f32;
        let img_height_f = img.height() as f32;

        // Ratio is used for calculating one dimension from another.
        let res_ratio = img_width_f as f32 / img_height_f as f32;

        let width =
            ((crossterm::terminal::size().unwrap_or_default().0 / 4) as f32 * self.zoom) as u32;
        let height = (width as f32 / res_ratio) as u32;

        // Scaled down image data.
        let scaled = img
            .resize_exact(width, height, FilterType::Nearest)
            .to_rgba8();

        // New scaled-down image's resolution.
        let size_w = scaled.width();
        let size_h = scaled.height();

        // Size of currently opened terminal.
        let terminal_size = crossterm::terminal::size().unwrap_or_default();
        // println!("{} {}", size_h, terminal_size.1);
        // origin for drawing the image onto screen
        let origin = (
            (terminal_size.0 / 2) as i16 - (size_w / 2) as i16 + self.offset_x,
            // divided by 4, because 1 line stores 2 pixels, so 50% space is spared!
            (terminal_size.1 / 2) as i16 - (size_h / 4) as i16 + self.offset_y,
        );

        // Iterator over rows of the image.
        let mut rows = scaled.rows();

        let mut stdout = std::io::stdout();

        // Current Y-coordinate in our "iteration".
        let mut current_y = 0;

        // CONTENT WARNING : Following lines of code are straight up disgusting - viewer's discretion
        // is adviced!

        // Please don't kill me for this, I know it is awful.

        while let Some(_) = rows.next() {
            // Long story short, one iteration is taking 2 rows, but these iterations are never used -
            // they are here just for counting!
            let n = rows.next();

            // Iterates over x coordinate.
            for x in 0..scaled.width() {
                let pos_x = x as i16 + origin.0;
                let pos_y = (current_y / 2) as i16 + origin.1;

                // Checks if given pixel can be rendered to screen.
                if pos_x < 0
                    || pos_y < 0
                    || pos_x >= terminal_size.0 as i16
                    || pos_y >= terminal_size.1 as i16
                {
                    continue;
                }

                let mut style = owo_colors::Style::new();

                match &n {
                    Some(_) => {
                        let px = scaled.get_pixel(x, current_y + 1);

                        if px[3] == 0 {
                            continue;
                        }

                        style = style.on_color(owo_colors::Rgb(px[0], px[1], px[2]));
                    }
                    None => (),
                };
                let px = scaled.get_pixel(x, current_y);
                let style = style.color(owo_colors::Rgb(px[0], px[1], px[2]));

                execute!(
                    stdout,
                    MoveTo(
                        (x as i32 + origin.0 as i32) as u16,
                        ((current_y / 2) as i32 + origin.1 as i32) as u16
                    )
                )
                .unwrap();

                stdout
                    .write_fmt(format_args!("{}", '▀'.style(style)))
                    .unwrap();
            }

            current_y += 2;
        }

        stdout.flush().unwrap();

        Ok(())
    }

    pub fn clipboard(&self) {
        let mut clip = Clipboard::new().unwrap();
        let img = self.get_current_image_data()
            .to_rgba8();

        let img_data = ImageData {
            width: img.width() as usize,
            height: img.height() as usize,
            bytes: Cow::from(img.as_bytes()),
        };
        clip.set_image(img_data).unwrap();
    }
}

pub fn safe_exit(err: &str) {
    let mut stdout = std::io::stdout();

    #[cfg(target_family = "unix")]
    execute!(stdout, PopKeyboardEnhancementFlags).unwrap();

    execute!(stdout, LeaveAlternateScreen).unwrap();

    disable_raw_mode().unwrap();

    println!("Uh-oh! Spectrate did a lil oopsie!\nReason: {}", err);

    std::process::exit(-1);
}
