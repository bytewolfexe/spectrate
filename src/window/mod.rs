#![allow(dead_code)]

enum WindowKind {
    Classic,
}

struct Window {
    title:String,
    text:String,
    kind:WindowKind,
}

impl Window {
    fn new(title:String, text:String, kind:WindowKind) -> Self {
        Self {
            title, text, kind
        }
    }

    fn draw(&self) {
        const WINDOW_WIDTH:u32 = 72;
        const WINDOW_HEIGHT:u32 = 32;

        let _title_len = self.title.len();
        let _text_len = self.text.len();


    }
}
