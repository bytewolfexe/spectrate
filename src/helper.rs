use crate::State;

use crossterm::{
    terminal::{
        enable_raw_mode, disable_raw_mode,
        ClearType, Clear, supports_keyboard_enhancement,
    },
    cursor::MoveTo,
    execute,
    event::{
        PopKeyboardEnhancementFlags,
        PushKeyboardEnhancementFlags,
        KeyboardEnhancementFlags,
    },
};


pub fn check_format(name: &str) -> bool {
    name.ends_with(".png")
        || name.ends_with(".jpg")
        || name.ends_with(".jpeg")
        || name.ends_with(".bmp")
        || name.ends_with(".tiff")
        || name.ends_with(".ico")
        || name.ends_with(".tga")
        || name.ends_with(".avif")
        || name.ends_with(".webp")
}


impl State {
    // Saves image into $HOME/Pictures directory.
    // TODO check if Pictures folder exists.
    pub fn save_image(&self) {
        let mut stdout = std::io::stdout();
        let image = self.get_current_image_data();

        // Prepare output buffer for standard processing - clearing buffer and moving cursor to the
        // beginning.
        execute!(stdout, Clear(ClearType::All), MoveTo(0, 0)).unwrap();

        if supports_keyboard_enhancement().unwrap() {
            execute!(stdout, PopKeyboardEnhancementFlags).unwrap();
        }

        // Disabling raw mode, entering standard input mode.
        disable_raw_mode().unwrap();

        let mut filename = String::new();

        // Loops until correct input is given.
        loop {
            // Reading input.
            println!("Enter name with extension : ");

            filename.clear();
            std::io::stdin().read_line(&mut filename).unwrap();

            // Removing newline character from received input.
            filename = filename.chars().filter(|c| c != &'\n').collect();

            // If filename is empty, it means user decided to not save the image after all (what a
            // shame).
            if filename.is_empty() {
                break;
            } else if !check_format(filename.as_str()) {
                println!("Bad file extension! Please try again ...");
                continue;
            }

            let final_path = home::home_dir().unwrap().join("Pictures");

            // Creating directories if they do not exist.
            std::fs::create_dir_all(&final_path).unwrap();

            // Assembling final path of the image.
            let final_path = final_path.join(filename.clone());

            // Try saving the image.
            println!("Saving image, please wait ...");
            match image.save(final_path.as_path()) {
                Ok(()) => break,
                Err(_) => {
                    println!("Something went wrong ... Please try again!");
                    continue;
                }
            }
        }

        // Leaving standard input mode, re-entering raw mode.
        enable_raw_mode().unwrap();

        if supports_keyboard_enhancement().unwrap() {
            execute!(
                stdout,
                PushKeyboardEnhancementFlags(KeyboardEnhancementFlags::REPORT_EVENT_TYPES)
            )
            .unwrap();
        }
    }
}
