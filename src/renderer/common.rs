
pub trait Rendering {
   fn draw_image(x: i16, y: i16, width: u32, height: u32, image: &image::DynamicImage);
}

pub struct Renderer;
