use super::common::*;

use crossterm::{execute, cursor::MoveTo};

use viuer::Config;

impl Rendering for Renderer {
    fn draw_image(x: i16, y: i16, width: u32, height: u32, image: &image::DynamicImage) {
        execute!(std::io::stdout(), MoveTo(0, 0)).unwrap();
        let mut config = Config {
            x: x as u16,
            y,
            width: Some(width),
            height: Some(height / 2),
            absolute_offset: false,
            transparent: true,
            restore_cursor: false,
            truecolor: true,
            use_kitty: false,
            use_iterm: false,
            use_sixel: false
        };
        if viuer::get_kitty_support() == viuer::KittySupport::Local {
            println!("Kitty!");
            config.use_kitty = true;
        } else if viuer::is_iterm_supported() {
            println!("ITERM!");
            config.use_iterm = true;
        } else if viuer::is_sixel_supported() {
            println!("SIXEL!");
            config.use_sixel = true;
        }

        viuer::print(image, &config).unwrap();
    }
}
