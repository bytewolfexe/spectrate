use crossterm::cursor::MoveTo;
use crossterm::event::{
    Event, KeyCode, KeyEventKind, KeyboardEnhancementFlags, PopKeyboardEnhancementFlags,
    PushKeyboardEnhancementFlags,
};
use crossterm::execute;
use crossterm::terminal::{
    disable_raw_mode, enable_raw_mode, size, Clear, ClearType, EnterAlternateScreen,
    LeaveAlternateScreen, supports_keyboard_enhancement,
};
use owo_colors::OwoColorize;
use spectrate::{safe_exit, State};
use std::io::prelude::*;

// TODO Comment this thing until it's too late ...


#[allow(dead_code)]
fn print_notif(msg: &str) {
    let terminal_size = size().unwrap();

    let msg_len = msg.len() as u16;

    let start_pos_x = terminal_size.0 - msg_len - 4;

    let mut stdout = std::io::stdout();

    execute!(stdout, MoveTo(start_pos_x, 6)).unwrap();

    stdout.write_fmt(format_args!("{}", msg.yellow())).unwrap();

    stdout.flush().unwrap();
}


// Renders bar at the bottom of the screen, displaying available commands.
fn draw_hint_bar(filename: String) {
    let s = size().unwrap();
    let mut stdout = std::io::stdout();
    execute!(stdout, MoveTo(0, s.1 - 1)).unwrap();
    stdout
        .write_fmt(format_args!("File : {}", filename))
        .unwrap();
    stdout.flush().unwrap();
}

fn redraw_image(state: &State) {
    execute!(std::io::stdout(), Clear(ClearType::All)).unwrap();
    match state.draw_current_image() {
        Ok(_) => (),
        Err(_) => return,
    }
    draw_hint_bar(state.get_current_image_name());
}

fn main() {
    // Illegal?!
    std::panic::set_hook(Box::new(|panic_info| {
        safe_exit(format!("{}", panic_info).as_str());
    }));
    let args: Vec<_> = std::env::args().collect();

    // Determining working path.
    let path = if args.len() > 1 {
        let tmp = String::new();
        let arg = args.last().unwrap_or(&tmp);
        let path = std::env::current_dir().unwrap_or_default();
        path.join(arg)
    } else {
        std::env::current_dir().unwrap_or_default()
    };

    // Loading images.
    let mut state = match State::load_dir(&path) {
        Ok(s) => s,
        Err(err) => {
            safe_exit(err.as_str());
            return;
        }
    };

    // Enables raw mode for raw input processing
    enable_raw_mode().unwrap();

    // Sets up terminal
    execute!(
        std::io::stdout(),
        EnterAlternateScreen,
        Clear(ClearType::All),
        // removed bcs of cross-compilation problems?
        // PushKeyboardEnhancementFlags(KeyboardEnhancementFlags::empty())
    )
    .unwrap();

    // Keyboard enhancements detection and enabling.
    if supports_keyboard_enhancement().unwrap() {
        execute!(
            std::io::stdout(),
            PushKeyboardEnhancementFlags(KeyboardEnhancementFlags::REPORT_EVENT_TYPES)
        )
        .unwrap();
    }

    // First redraw to actually show stuff.
    redraw_image(&state);

    print_notif(format!("Loaded {} images", state.len()).as_str());

    // Main event loop
    loop {
        if crossterm::event::poll(std::time::Duration::from_millis(100)).unwrap() {
            let event = crossterm::event::read().unwrap();
            match event {
                // Keys control
                Event::Key(key_event) => {
                    if KeyEventKind::Release == key_event.kind {
                        match key_event.code {
                            KeyCode::Char('q') => {
                                break;
                            }
                            KeyCode::Char('h') | KeyCode::Left => {
                                state.previous_image();
                                state.reset();
                                redraw_image(&state);
                            }

                            KeyCode::Char('l') | KeyCode::Right => {
                                state.next_image();
                                state.reset();
                                redraw_image(&state);
                            }
                            KeyCode::Char('k') | KeyCode::Up => {
                                state.zoom_in();
                                redraw_image(&state);
                            }
                            KeyCode::Char('j') | KeyCode::Down => {
                                state.zoom_out();
                                redraw_image(&state);
                            }
                            KeyCode::Char('r') => {
                                redraw_image(&state);
                            }
                            KeyCode::Char('w') => {
                                state.move_image(0, 10);
                                redraw_image(&state);
                            }
                            KeyCode::Char('s') => {
                                state.move_image(0, -10);
                                redraw_image(&state);
                            }
                            KeyCode::Char('a') => {
                                state.move_image(10, 0);
                                redraw_image(&state);
                            }
                            KeyCode::Char('d') => {
                                state.move_image(-10, 0);
                                redraw_image(&state);
                            }
                            KeyCode::Char('z') => {
                                state.save_image();
                                redraw_image(&state);
                            }
                            KeyCode::Char('c') => {
                                state.clipboard();
                            }
                            _ => (),
                        }
                    }
                }
                _ => (),
            }
        }
    }

    // Clean up
    #[cfg(target_family = "unix")]
    execute!(std::io::stdout(), PopKeyboardEnhancementFlags).unwrap();

    execute!(std::io::stdout(), LeaveAlternateScreen).unwrap();

    // Disables raw mode
    disable_raw_mode().unwrap();
}
