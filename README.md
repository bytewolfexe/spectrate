# Spectrate

### [Note] : This project was abandoned - it is getting replaced by new project soon!

## Yet another simple utility for viewing images in command line, written in Rust

### Introduction

Spectrate is an image-viewing utility for command lines made in Rust.

### Starting spectrate

Firstly, download and compile the source code using the following command:

```
[user@linux]$ cargo build --release
```

Then place the compiled application executable into your PATH, so you can access it from your command line. You may need to restart any running terminals to apply changes.

If you do this correctly, you can run `spectrate` command, which will open spectrate
in terminal's current working directory.

You can also manually specify path where you want spectrate to open using `spectrate [PATH]`,
for example on UNIX/Linux:

```
[user@linux]$ spectrate ~/Pictures/Screenshots
```

### Controls

Spectrate controls are inspired by vim. Here is a list of all keybinds:

- using `W/A/S/D` keys, you move image around
  
  > WARNING : Moving image around is currently not restricted, so you can move it out of viewing bounds!

- using `h` / `l` keys or ARROW LEFT / ARROW RIGHT keys, you switch between images.

- using `j` / `k` keys or ARROW DOWN / ARROW UP, you zoom out / in respectively.

- using `r` key, you reload image to default position and scale.

- using `z` key, you can save image in `<HOME>/Pictures` folder. You need to specify filename with extension!

- using `q` key, you exit spectrate.

- using `c` key, you can copy image into your clipboard.

### Planned features

- *s p i n* images 

- Lua / Toml plugins (maybe)

- Filters, like grayscale, invert color, etc.

### Found bugs

- There were no bugs noticed since last commit. If you notice anything, please let me know by opening an issue.
